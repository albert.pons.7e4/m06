package Model;

import java.util.Collection;

public class ProducteEntity {
    private int prodNum;
    private String descripcio;
    private Collection<DetallEntity> detallsByProdNum;

    public int getProdNum() {
        return prodNum;
    }

    public void setProdNum(int prodNum) {
        this.prodNum = prodNum;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProducteEntity that = (ProducteEntity) o;

        if (prodNum != that.prodNum) return false;
        if (descripcio != null ? !descripcio.equals(that.descripcio) : that.descripcio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prodNum;
        result = 31 * result + (descripcio != null ? descripcio.hashCode() : 0);
        return result;
    }

    public Collection<DetallEntity> getDetallsByProdNum() {
        return detallsByProdNum;
    }

    public void setDetallsByProdNum(Collection<DetallEntity> detallsByProdNum) {
        this.detallsByProdNum = detallsByProdNum;
    }

    @Override
    public String toString() {
        return "ProducteEntity{" +
                "prodNum=" + prodNum +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
