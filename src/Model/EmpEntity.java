package Model;

import java.math.BigInteger;
import java.sql.Date;
import java.util.Collection;

public class EmpEntity {
    private int empNo;
    private String cognom;
    private String ofici;
    private Date dataAlta;
    private BigInteger salari;
    private BigInteger comissio;
    private Collection<ClientEntity> clientsByEmpNo;
    private EmpEntity empByCap;
    private Collection<EmpEntity> empsByEmpNo;
    private DeptEntity deptByDeptNo;

    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public Date getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(Date dataAlta) {
        this.dataAlta = dataAlta;
    }

    public BigInteger getSalari() {
        return salari;
    }

    public void setSalari(BigInteger salari) {
        this.salari = salari;
    }

    public BigInteger getComissio() {
        return comissio;
    }

    public void setComissio(BigInteger comissio) {
        this.comissio = comissio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmpEntity empEntity = (EmpEntity) o;

        if (empNo != empEntity.empNo) return false;
        if (cognom != null ? !cognom.equals(empEntity.cognom) : empEntity.cognom != null) return false;
        if (ofici != null ? !ofici.equals(empEntity.ofici) : empEntity.ofici != null) return false;
        if (dataAlta != null ? !dataAlta.equals(empEntity.dataAlta) : empEntity.dataAlta != null) return false;
        if (salari != null ? !salari.equals(empEntity.salari) : empEntity.salari != null) return false;
        if (comissio != null ? !comissio.equals(empEntity.comissio) : empEntity.comissio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = empNo;
        result = 31 * result + (cognom != null ? cognom.hashCode() : 0);
        result = 31 * result + (ofici != null ? ofici.hashCode() : 0);
        result = 31 * result + (dataAlta != null ? dataAlta.hashCode() : 0);
        result = 31 * result + (salari != null ? salari.hashCode() : 0);
        result = 31 * result + (comissio != null ? comissio.hashCode() : 0);
        return result;
    }

    public Collection<ClientEntity> getClientsByEmpNo() {
        return clientsByEmpNo;
    }

    public void setClientsByEmpNo(Collection<ClientEntity> clientsByEmpNo) {
        this.clientsByEmpNo = clientsByEmpNo;
    }

    public EmpEntity getEmpByCap() {
        return empByCap;
    }

    public void setEmpByCap(EmpEntity empByCap) {
        this.empByCap = empByCap;
    }

    public Collection<EmpEntity> getEmpsByEmpNo() {
        return empsByEmpNo;
    }

    public void setEmpsByEmpNo(Collection<EmpEntity> empsByEmpNo) {
        this.empsByEmpNo = empsByEmpNo;
    }

    public DeptEntity getDeptByDeptNo() {
        return deptByDeptNo;
    }

    public void setDeptByDeptNo(DeptEntity deptByDeptNo) {
        this.deptByDeptNo = deptByDeptNo;
    }

    @Override
    public String toString() {
        return "EmpEntity{" +
                "empNo=" + empNo +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", dataAlta=" + dataAlta +
                ", salari=" + salari +
                ", comissio=" + comissio +
                ", empByCap=" + empByCap +
                ", deptByDeptNo=" + deptByDeptNo +
                '}';
    }
}
