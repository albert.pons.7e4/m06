package Model;

import java.math.BigInteger;
import java.util.Collection;

public class DeptEntity {
    private BigInteger deptNo;
    private String dnom;
    private String loc;
    private Collection<EmpEntity> empsByDeptNo;

    public BigInteger getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(BigInteger deptNo) {
        this.deptNo = deptNo;
    }

    public String getDnom() {
        return dnom;
    }

    public void setDnom(String dnom) {
        this.dnom = dnom;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeptEntity that = (DeptEntity) o;

        if (deptNo != null ? !deptNo.equals(that.deptNo) : that.deptNo != null) return false;
        if (dnom != null ? !dnom.equals(that.dnom) : that.dnom != null) return false;
        if (loc != null ? !loc.equals(that.loc) : that.loc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deptNo != null ? deptNo.hashCode() : 0;
        result = 31 * result + (dnom != null ? dnom.hashCode() : 0);
        result = 31 * result + (loc != null ? loc.hashCode() : 0);
        return result;
    }

    public Collection<EmpEntity> getEmpsByDeptNo() {
        return empsByDeptNo;
    }

    public void setEmpsByDeptNo(Collection<EmpEntity> empsByDeptNo) {
        this.empsByDeptNo = empsByDeptNo;
    }

    @Override
    public String toString() {
        return "DeptEntity{" +
                "deptNo=" + deptNo +
                ", dnom='" + dnom + '\'' +
                ", loc='" + loc + '\'' +
                '}';
    }
}
