package Model;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;

public class ComandaEntity {
    private int comNum;
    private Date comData;
    private String comTipus;
    private Date dataTramesa;
    private BigDecimal total;

    @Override
    public String toString() {
        return "ComandaEntity{" +
                "comNum=" + comNum +
                ", comData=" + comData +
                ", comTipus='" + comTipus + '\'' +
                ", dataTramesa=" + dataTramesa +
                ", total=" + total +
                ", clientByClientCod=" + clientByClientCod +
                '}';
    }

    private ClientEntity clientByClientCod;
    private Collection<DetallEntity> detallsByComNum;

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public Date getComData() {
        return comData;
    }

    public void setComData(Date comData) {
        this.comData = comData;
    }

    public String getComTipus() {
        return comTipus;
    }

    public void setComTipus(String comTipus) {
        this.comTipus = comTipus;
    }

    public Date getDataTramesa() {
        return dataTramesa;
    }

    public void setDataTramesa(Date dataTramesa) {
        this.dataTramesa = dataTramesa;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComandaEntity that = (ComandaEntity) o;

        if (comNum != that.comNum) return false;
        if (comData != null ? !comData.equals(that.comData) : that.comData != null) return false;
        if (comTipus != null ? !comTipus.equals(that.comTipus) : that.comTipus != null) return false;
        if (dataTramesa != null ? !dataTramesa.equals(that.dataTramesa) : that.dataTramesa != null) return false;
        if (total != null ? !total.equals(that.total) : that.total != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = comNum;
        result = 31 * result + (comData != null ? comData.hashCode() : 0);
        result = 31 * result + (comTipus != null ? comTipus.hashCode() : 0);
        result = 31 * result + (dataTramesa != null ? dataTramesa.hashCode() : 0);
        result = 31 * result + (total != null ? total.hashCode() : 0);
        return result;
    }

    public ClientEntity getClientByClientCod() {
        return clientByClientCod;
    }

    public void setClientByClientCod(ClientEntity clientByClientCod) {
        this.clientByClientCod = clientByClientCod;
    }

    public Collection<DetallEntity> getDetallsByComNum() {
        return detallsByComNum;
    }

    public void setDetallsByComNum(Collection<DetallEntity> detallsByComNum) {
        this.detallsByComNum = detallsByComNum;
    }
}
