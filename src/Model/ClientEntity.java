package Model;

import java.math.BigDecimal;
import java.util.Collection;

public class ClientEntity {
    private int clientCod;
    private String nom;
    private String adreca;
    private String ciutat;
    private String estat;
    private String codiPostal;
    private Integer area;
    private String telefon;
    private BigDecimal limitCredit;
    private String observacions;
    private EmpEntity empByReprCod;
    private Collection<ComandaEntity> comandasByClientCod;

    public int getClientCod() {
        return clientCod;
    }

    public void setClientCod(int clientCod) {
        this.clientCod = clientCod;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public String getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(String codiPostal) {
        this.codiPostal = codiPostal;
    }

    public Integer getArea() {
        return area;
    }

    public void setArea(Integer area) {
        this.area = area;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public BigDecimal getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(BigDecimal limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientEntity that = (ClientEntity) o;

        if (clientCod != that.clientCod) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (adreca != null ? !adreca.equals(that.adreca) : that.adreca != null) return false;
        if (ciutat != null ? !ciutat.equals(that.ciutat) : that.ciutat != null) return false;
        if (estat != null ? !estat.equals(that.estat) : that.estat != null) return false;
        if (codiPostal != null ? !codiPostal.equals(that.codiPostal) : that.codiPostal != null) return false;
        if (area != null ? !area.equals(that.area) : that.area != null) return false;
        if (telefon != null ? !telefon.equals(that.telefon) : that.telefon != null) return false;
        if (limitCredit != null ? !limitCredit.equals(that.limitCredit) : that.limitCredit != null) return false;
        if (observacions != null ? !observacions.equals(that.observacions) : that.observacions != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = clientCod;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (adreca != null ? adreca.hashCode() : 0);
        result = 31 * result + (ciutat != null ? ciutat.hashCode() : 0);
        result = 31 * result + (estat != null ? estat.hashCode() : 0);
        result = 31 * result + (codiPostal != null ? codiPostal.hashCode() : 0);
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (telefon != null ? telefon.hashCode() : 0);
        result = 31 * result + (limitCredit != null ? limitCredit.hashCode() : 0);
        result = 31 * result + (observacions != null ? observacions.hashCode() : 0);
        return result;
    }

    public EmpEntity getEmpByReprCod() {
        return empByReprCod;
    }

    public void setEmpByReprCod(EmpEntity empByReprCod) {
        this.empByReprCod = empByReprCod;
    }

    public Collection<ComandaEntity> getComandasByClientCod() {
        return comandasByClientCod;
    }

    public void setComandasByClientCod(Collection<ComandaEntity> comandasByClientCod) {
        this.comandasByClientCod = comandasByClientCod;
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "clientCod=" + clientCod +
                ", nom='" + nom + '\'' +
                ", adreca='" + adreca + '\'' +
                ", ciutat='" + ciutat + '\'' +
                ", estat='" + estat + '\'' +
                ", codiPostal='" + codiPostal + '\'' +
                ", area=" + area +
                ", telefon='" + telefon + '\'' +
                ", limitCredit=" + limitCredit +
                ", observacions='" + observacions + '\'' +
                ", empByReprCod=" + empByReprCod +
                '}';
    }
}
