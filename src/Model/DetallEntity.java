package Model;

import java.math.BigDecimal;

public class DetallEntity {
    private int detallNum;
    private BigDecimal preuVenda;
    private Integer quantitat;
    private BigDecimal importe;
    private ComandaEntity comandaByComNum;
    private ProducteEntity producteByProdNum;

    public int getDetallNum() {
        return detallNum;
    }

    public void setDetallNum(int detallNum) {
        this.detallNum = detallNum;
    }

    public BigDecimal getPreuVenda() {
        return preuVenda;
    }

    public void setPreuVenda(BigDecimal preuVenda) {
        this.preuVenda = preuVenda;
    }

    public Integer getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(Integer quantitat) {
        this.quantitat = quantitat;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DetallEntity that = (DetallEntity) o;

        if (detallNum != that.detallNum) return false;
        if (preuVenda != null ? !preuVenda.equals(that.preuVenda) : that.preuVenda != null) return false;
        if (quantitat != null ? !quantitat.equals(that.quantitat) : that.quantitat != null) return false;
        if (importe != null ? !importe.equals(that.importe) : that.importe != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = detallNum;
        result = 31 * result + (preuVenda != null ? preuVenda.hashCode() : 0);
        result = 31 * result + (quantitat != null ? quantitat.hashCode() : 0);
        result = 31 * result + (importe != null ? importe.hashCode() : 0);
        return result;
    }

    public ComandaEntity getComandaByComNum() {
        return comandaByComNum;
    }

    public void setComandaByComNum(ComandaEntity comandaByComNum) {
        this.comandaByComNum = comandaByComNum;
    }

    public ProducteEntity getProducteByProdNum() {
        return producteByProdNum;
    }

    public void setProducteByProdNum(ProducteEntity producteByProdNum) {
        this.producteByProdNum = producteByProdNum;
    }

    @Override
    public String toString() {
        return "DetallEntity{" +
                "detallNum=" + detallNum +
                ", preuVenda=" + preuVenda +
                ", quantitat=" + quantitat +
                ", importe=" + importe +
                ", comandaByComNum=" + comandaByComNum +
                ", producteByProdNum=" + producteByProdNum +
                '}';
    }
}
